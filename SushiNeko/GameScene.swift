//
//  GameScene.swift
//  SushiNeko
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-10-16.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import SpriteKit
import GameplayKit

/* Tracking enum for use with character and sushi side */
enum Side {
    case left, right, none
}

/* Game objects */
var sushiBasePiece: SushiPiece!
/* Cat Character */
var character: Character!
/* Sushi tower array */
var sushiTower: [SushiPiece] = []

class GameScene: SKScene {
    
    override func didMove(to view: SKView) {
        /* Connect game objects */
        sushiBasePiece = (childNode(withName: "sushiBasePiece") as! SushiPiece)
        character = (childNode(withName: "character") as! Character)

        
        /* Setup chopstick connections */
        sushiBasePiece.connectChopsticks()
        
//        /* Manually stack the start of the tower */
//        addTowerPiece(side: .none)
//        addTowerPiece(side: .right)

        self.addRandomPieces(total: 10)
    }
    
    func addTowerPiece(side: Side) {
        /* Add a new sushi piece to the sushi tower */
        /* Copy original sushi piece */
        let newPiece = sushiBasePiece.copy() as! SushiPiece
        newPiece.connectChopsticks()

        /* Access last piece properties */
        let lastPiece = sushiTower.last

        /* Add on top of last piece, default on first piece */
        let lastPosition = lastPiece?.position ?? sushiBasePiece.position
        newPiece.position.x = lastPosition.x
        newPiece.position.y = lastPosition.y + 100

        /* Increment Z to ensure it's on top of the last piece, default on first piece*/
        let lastZPosition = lastPiece?.zPosition ?? sushiBasePiece.zPosition
        newPiece.zPosition = lastZPosition + 1

            /* Set side */
            newPiece.side = side

            /* Add sushi to scene */
            addChild(newPiece)

            /* Add sushi piece to the sushi tower */
            sushiTower.append(newPiece)
    }
    
    func addRandomPieces(total: Int) {
        /* Add random sushi pieces to the sushi tower */
        
        
        
        for _ in 1...total {
            
//
//            /* Need to access last piece properties */
//            let lastPiece = sushiTower.last
//
            // This way is better than the original
            let rand = Int.random(in: 0...2)
            if rand == 1 {
                addTowerPiece(side: .left)
            }
            else if rand == 2 {
                addTowerPiece(side: .right)
            }
            else {
                addTowerPiece(side: .none)
            }
            
//            // Need to ensure we don't create impossible sushi structures
//            if lastPiece?.side != .none {
//                addTowerPiece(side: .none)
//            } else {
//
//                /* Random Number Generator */
//                let rand = arc4random_uniform(100)
//
//                if rand < 45 {
//                    /* 45% Chance of a left piece */
//                    addTowerPiece(side: .left)
//                } else if rand < 90 {
//                    /* 45% Chance of a right piece */
//                    addTowerPiece(side: .right)
//                } else {
//                    /* 10% Chance of an empty piece */
//                    addTowerPiece(side: .none)
//                }
//            }
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        /* Called when a touch begins */
        
        /* We only need a single touch here */
        let touch = touches.first!
        
        /* Get touch position in scene */
        let location = touch.location(in: self)
        
        /* Was touch on left/right hand side of screen? */
        if location.x > size.width / 2 {
            character.side = .right
        } else {
            character.side = .left
        }
        
        /* Grab sushi piece on top of the base sushi piece, it will always be 'first' */
        if let firstPiece = sushiTower.first {
            /* Remove from sushi tower array */
            sushiTower.removeFirst()
            firstPiece.removeFromParent()
            
            /* Add a new sushi piece to the top of the sushi tower */
            addRandomPieces(total: 1)
        }
        
        // ------------------------------------
        // MARK: ANIMATION OF PUNCHING CAT
        // -------------------------------------
        
        // show animation of cat punching tower
        let image1 = SKTexture(imageNamed: "character1")
        let image2 = SKTexture(imageNamed: "character2")
        let image3 = SKTexture(imageNamed: "character3")
        
        let punchTextures = [image1, image2, image3, image1]
        
        let punchAnimation = SKAction.animate(
            with: punchTextures,
            timePerFrame: 0.1)
        
        character.run(punchAnimation)
        
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
    }
}
