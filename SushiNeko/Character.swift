//
//  Character.swift
//  SushiNeko
//
//  Created by Antonio Merendaz do Carmo Nt on 2019-10-16.
//  Copyright © 2019 Merendaz. All rights reserved.
//

import SpriteKit

class Character: SKSpriteNode {
    
    /* Character side */
    var side: Side = .left {
        didSet {
            if side == .left {
                xScale = 2.5
                yScale = 3.0
                position.x = -50
            } else {
                /* An easy way to flip an asset horizontally is to invert the X-axis scale */
                xScale = -2.5
                yScale = 3.0
                position.x = 800
            }
        }
    }
    
    /* You are required to implement this for your subclass to work */
    override init(texture: SKTexture?, color: UIColor, size: CGSize) {
        super.init(texture: texture, color: color, size: size)
    }
    
    /* You are required to implement this for your subclass to work */
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
